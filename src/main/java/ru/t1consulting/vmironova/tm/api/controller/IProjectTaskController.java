package ru.t1consulting.vmironova.tm.api.controller;

public interface IProjectTaskController {

    void bindTaskToProject();

    void unbindTaskFromProject();

}
