package ru.t1consulting.vmironova.tm.api.repository;

import ru.t1consulting.vmironova.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
