package ru.t1consulting.vmironova.tm.api.service;

import ru.t1consulting.vmironova.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
